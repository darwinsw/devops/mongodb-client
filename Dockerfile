FROM ubuntu:20.10
RUN apt update && apt install -y gnupg wget coreutils iputils-ping && wget -qO - https://www.mongodb.org/static/pgp/server-4.4.asc | apt-key add - && apt clean
RUN echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/4.4 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.4.list
RUN apt update &&  apt install -y mongodb-org-shell mongodb-org-tools mongodb-org-database-tools-extra && apt clean
